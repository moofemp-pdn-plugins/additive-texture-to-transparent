# Additive Texture to Transparent for Paint.NET

Creates a translucent approximation of what an additive texture could be on a transparent background

[`Additive Texture to Transparent.cs`](Additive%20Texture%20to%20Transparent.cs) is the source code  
[`Additive Texture to Transparent.dll`](Additive%20Texture%20to%20Transparent.dll) is the compiled .dll  
[`logo.png`](logo.png) is the effect icon

**INSTALLATION INSTRUCTIONS:**  
You only need to download [`Additive Texture to Transparent.dll`](Additive%20Texture%20to%20Transparent.dll) and place it in `C:\Program Files\paint.net\Effects`

Created with CodeLab: https://boltbait.com/pdn/codelab/

Paint.NET: https://www.getpaint.net/

## Sample Images

Click any of these to enlarge; they're bigger than shown here.

Original image; sprite specifically made for this demonstration  
<img src="samples/orig.png" alt="orig" width=150>

After running the effect  
<img src="samples/transparent.png" alt="transparent" width=150>

Overlaid on a sample background  
<img src="samples/overlaid.png" alt="overlaid" width=150>

Control image showing the original sprite with Additive blend mode over the sample background; notice that the effect doesn't perfectly match but it's pretty close  
<img src="samples/control.png" alt="control" width=150>
