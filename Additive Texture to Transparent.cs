// Name: Moof's Additive Texture to Transparent v1.0
// Submenu: Color
// Author: MoofEMP
// Title: Additive Texture to Transparent
// Version: 1.0
// Desc: Creates a translucent approximation of what an additive texture could be on a transparent background
// URL: https://gitlab.com/moofemp-pdn-plugins/additive-texture-to-transparent

void Render(Surface dst,Surface src,Rectangle rect){
    ColorBgra CurrentPixel;
    for(int y=rect.Top;y<rect.Bottom;y++){
        if(IsCancelRequested) return;
        for(int x=rect.Left;x<rect.Right;x++){
            CurrentPixel=src[x,y];

            HsvColor pxhsv=HsvColor.FromColor(CurrentPixel.ToColor());  //converting RGB to HSV
            int oldValue=pxhsv.Value;
            pxhsv.Value=100;
            ColorBgra NewPixel=ColorBgra.FromColor(pxhsv.ToColor());    //HSV back to RGB
            NewPixel.A=(byte)(2.55*oldValue);

            dst[x,y]=NewPixel;
        }
    }
}
